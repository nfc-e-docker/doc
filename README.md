# Documentação do Projeto

Scripts de construção (build), inicialização (start) e execução de testes (test) da plataforma NFC-e.

## Requisitos para execução local:

- Node JS instalado na máquina;
- Container MongoDB disponível (especificar a url de conexão no arquivo `.env` por meio da variável de ambiente `MONGO_URL` em cada projeto;
- Executar a instalação de dependências em cada um dos módulos por meio do comando `npm install`.

Scripts com extensão `sh` e `bat` são destinados aos ambientes Linux e Windows respectivamente.

**_Obs.: Os scripts devem estar localizados na raiz do projeto conforme apresenta a estrutura abaixo:_**

```text
├── root-folder                     # Root directory
│   ├── ba                          # BA service
│   ├── mg                          # MG service
│   └── ...                         # Other services
│   ├── mg                          # MG service
│   └── ...                         # Other services
│   ├── integrated_test_linux.sh    # Integrated tests script
│   └── normal_test_linux.sh        # Normal test script
│   └── ...                         # Other scripts
```

## Descrição dos scripts:

- **integrated_test**: Inicia todos os serviços e executa os testes de forma integrada;

- **normal_test**: Inicia todos os serviços e executa testes de unidade em cada um deles;
- **start**: Inicia todos os serviços para operação de forma integrada.

A execução dos testes também pode ser feita manualmente:

- geração de documentação (projeto Gateway): `npm run doc`;
- análise de qualidade de código com ESLint: `npm run lint`;
- testes de unidade: `npm run test`.

### Documentação de interfaces

A documentação da API de acesso à plataforma encontra-se disponível em:
https://nfc-e-docker.gitlab.io/doc/
