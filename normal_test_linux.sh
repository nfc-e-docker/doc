echo "\n"
echo "\tStarting normal tests...\n"
echo "\tStarting up the Gateway..."
cd gateway
TEST_TYPE=normal npm run test &
echo "\tStarting up MG service..."
cd ..
cd mg
TEST_TYPE=normal npm run test &
echo "\tStarting up BA service..."
cd ..
cd ba
TEST_TYPE=normal npm run test &
echo "\tStarting up Signature service..."
cd ..
cd signature
TEST_TYPE=normal npm run test &
echo "\tStarting up DANFE service..."
cd ..
cd danfe
TEST_TYPE=normal npm run test