echo "\n"
echo "\tStarting integrated tests...\n"
echo "\tStarting up the Gateway..."
cd gateway
TEST_TYPE=integrated WAIT_FOR_SERVICES=15000 npm run test &
echo "\tStarting up BA service..."
cd ..
cd ba
TEST_TYPE=integrated node . &
echo "\tStarting up MG service..."
cd ..
cd mg
TEST_TYPE=integrated node . &
echo "\tStarting up Signature service..."
cd ..
cd signature
TEST_TYPE=integrated node . &
echo "\tStarting up DANFE service..."
cd ..
cd danfe
TEST_TYPE=integrated node .
