define({
  "name": "POC Plataforma NFC-e: Uma abordagem orientada a microsserviços",
  "version": "1.0.0",
  "description": "Prova de Conceito desenvolvida para o Trabalho Final de Graduação do curso de Arquitetura de Software Distribuído",
  "title": "Plataforma NFC-e: Uma abordagem orientada a microsserviços",
  "header": {
    "title": "Orientações gerais",
    "content": "<h2>Orientações Gerais</h2>\n<h2>Requisitos para execução local:</h2>\n<ul>\n<li>Node JS instalado na máquina;</li>\n<li>Container MongoDB disponível (especificar a url de conexão no arquivo <code>.env</code> por meio da variável de ambiente <code>MONGO_URL</code> em cada projeto;</li>\n<li>Executar a instalação de dependências em cada um dos módulos por meio do comando <code>npm install</code>.</li>\n</ul>\n<p>Scripts com extensão <code>sh</code> e <code>bat</code> são destinados aos ambientes Linux e Windows respectivamente.</p>\n<h2>Descrição dos scripts:</h2>\n<ul>\n<li>\n<p><strong>integrated_test</strong>: Inicia todos os serviços e executa os testes de forma integrada;</p>\n</li>\n<li>\n<p><strong>normal_test</strong>: Inicia todos os serviços e executa testes de unidade em cada um deles;</p>\n</li>\n<li>\n<p><strong>start</strong>: Inicia todos os serviços para operação de forma integrada.</p>\n</li>\n</ul>\n<p>A execução dos testes também pode ser feita manualmente:</p>\n<ul>\n<li>geração de documentação (projeto Gateway): <code>npm run doc</code>;</li>\n<li>análise de qualidade de código com ESLint: <code>npm run lint</code>;</li>\n<li>testes de unidade: <code>npm run test</code>.</li>\n</ul>\n<p><strong>Observações:</strong></p>\n<ul>\n<li>Ambas as requisições apresentadas abaixo operam com representação de dados em JSON ou XML;</li>\n<li>Caso teste a aplicação utilizando ferramentas externas como o Postman, Browser, HttpRequester, entre outras, lembre-se sempre de fornecer o token de acesso obtido na etapa de login no cabeçalho de cada requisição. Um exemplo de como se fazer isso via Postman pode ser conferido <a href=\"https://learning.getpostman.com/docs/postman/sending_api_requests/authorization/\">aqui</a>.</li>\n</ul>\n"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-08-30T13:57:55.682Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
