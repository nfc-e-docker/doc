@echo off
echo Starting services
echo.
echo Gateway
cd gateway
start  node .
timeout 3
echo MG
cd ..
cd mg
start node .
timeout 3
echo Signature
cd ..
cd signature
start node .
pause