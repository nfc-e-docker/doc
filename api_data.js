define({ "api": [
  {
    "type": "delete",
    "url": "/company",
    "title": "Remove uma empresa",
    "version": "1.0.0",
    "group": "Empresa",
    "description": "<p>Remove uma empresa da plataforma.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "n",
            "description": "<p>Quantidade de registros removidos. O valor esperado é que &quot;n&quot; seja sempre igual a um.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Resposta de sucesso:",
          "content": "HTTP/1.1 200 OK\n{\n  { \"n\": 1, \"ok\": 1 }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Resposta de erro genérica:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n     Internal Server Error\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cnpj",
            "description": "<p>CNPJ da empresa.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemplo:",
        "content": "https://localhost:3019/company",
        "type": "json"
      }
    ],
    "filename": "routes/companyRoute.js",
    "groupTitle": "Empresa",
    "name": "DeleteCompany"
  },
  {
    "type": "get",
    "url": "/company/:cnpj",
    "title": "Obtém uma empresa",
    "version": "1.0.0",
    "group": "Empresa",
    "description": "<p>Obtém os dados de uma empresa cadastrada na plataforma.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "company",
            "description": "<p>Objeto que contém as informações cadastrais da empresa.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Resposta de sucesso:",
          "content": "HTTP/1.1 200 OK\n{\n  \"company\": \"Informações da empresa selecionada\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Resposta de erro (empresa não cadastrada):",
          "content": "HTTP/1.1 401 Not Found\n{\n     Unauthorized\n}",
          "type": "json"
        },
        {
          "title": "Resposta de erro genérica:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n     Internal Server Error\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cnpj",
            "description": "<p>CNPJ da empresa.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemplo:",
        "content": "https://localhost:3019/company/25906199000142",
        "type": "json"
      }
    ],
    "filename": "routes/companyRoute.js",
    "groupTitle": "Empresa",
    "name": "GetCompanyCnpj"
  },
  {
    "type": "post",
    "url": "/company",
    "title": "Cadastra uma empresa",
    "version": "1.0.0",
    "group": "Empresa",
    "description": "<p>Cadastra uma empresa na plataforma. Esse método é utilizado apenas para fins de teste. Em um cenário real de uso, o cadastro deve ser feito diretamente pelo administrador do sistema na base Mongo.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "insertedCount",
            "description": "<p>Quantidade de registros inseridos. O valor esperado é que &quot;insertedCount&quot; seja sempre igual a um.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Resposta de sucesso:",
          "content": "HTTP/1.1 200 OK\n{\n   \"insertedCount\": 1\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Resposta de erro genérica:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n     Internal Server Error\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cnpj",
            "description": "<p>CNPJ da empresa.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemplo:",
        "content": "https://localhost:3019/company",
        "type": "json"
      }
    ],
    "filename": "routes/companyRoute.js",
    "groupTitle": "Empresa",
    "name": "PostCompany"
  },
  {
    "type": "put",
    "url": "/company",
    "title": "Utualiza uma empresa",
    "version": "1.0.0",
    "group": "Empresa",
    "description": "<p>Atualiza as informações de empresa na plataforma, salvo o valor de CNPJ.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "nModified",
            "description": "<p>Quantidade de registros modificados. O valor esperado é que &quot;nModified&quot; seja sempre igual a um.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Resposta de sucesso:",
          "content": "HTTP/1.1 200 OK\n{\n  { \"n\": 1, \"nModified\": 1, \"ok\": 1 }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Resposta de erro genérica:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n     Internal Server Error\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "company",
            "description": "<p>Objeto com as informações a serem atualizadas. O envio do CNPJ é obrigatório - utilizado nas busca e validação de informações.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemplo:",
        "content": "https://localhost:3019/company",
        "type": "json"
      }
    ],
    "filename": "routes/companyRoute.js",
    "groupTitle": "Empresa",
    "name": "PutCompany"
  },
  {
    "type": "get",
    "url": "/log",
    "title": "Retorno de logs",
    "version": "1.0.0",
    "group": "Log",
    "description": "<p>Fornece o histórico de atividades executadas dentro da plataforma.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "array",
            "description": "<p>Conjunto de registros para o período especificado.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Resposta de sucesso:",
          "content": "HTTP/1.1 200 OK\n{\n  [ { _id: '5d602bc717ff1015fc8683aa',\n     url: '/company/25906199000142',\n      date: '2019-08-23T18:09:11.481Z' },\n    { _id: '5d602bc717ff1015fc8683ab',\n     url: '/company',\n      date: '2019-08-23T18:09:11.786Z' },\n   }]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Resposta de erro:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n     Internal Server Error\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "start",
            "description": "<p>Período inicial.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "end",
            "description": "<p>Período final.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Quantidade de registros a serem retornados.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemplo:",
        "content": "https://localhost:3019/log?start=2019-08-23T18:09:11.786Z&end=2019-08-23T18:09:11.786Z&limit=10",
        "type": "json"
      }
    ],
    "filename": "routes/logRoute.js",
    "groupTitle": "Log",
    "name": "GetLog"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Login de empresa",
    "version": "1.0.0",
    "group": "Login",
    "description": "<p>Realiza o login de uma empresa dentro da plataforma.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token de acesso válido pelas próximas 72 hrs.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Resposta de sucesso:",
          "content": "HTTP/1.1 200 OK\n{\n  eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJjbnBqIjoiMjU5MDYxOTkwMDAxNDIiLCJpYXQiOjE1NjcwODU4NzksImV4cCI6MTU2NzM0NTA3OX0.pbJ6J1C47r8yS6VD_SiiU_yJpDSoqoZqXcnyXOSwTbGqGP_yR8UImREmpkp9FDjbIpqosqKJka_wrVYnWP4Q_A\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Resposta de erro (empresa não cadastrada):",
          "content": "HTTP/1.1 404 Not Found\n{\n     Company not found\n}",
          "type": "json"
        },
        {
          "title": "Resposta de erro :",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n     Internal Server Error\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cnpj",
            "description": "<p>CNPJ da empresa.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "platformPassword",
            "description": "<p>Senha de acesso à plataforma.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemplo:",
        "content": "https://localhost:3019/login",
        "type": "json"
      }
    ],
    "filename": "routes/loginRoute.js",
    "groupTitle": "Login",
    "name": "PostLogin"
  },
  {
    "type": "delete",
    "url": "/nfce/cancel/:key",
    "title": "Canecelamento de uma NFC-e",
    "version": "1.0.0",
    "group": "Nfce",
    "description": "<p>Cancelamento de uma nota autorizada que se encontra armazenada na plataforma.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "File",
            "optional": false,
            "field": "pdf",
            "description": "<p>PDF do documento de DANFE.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Resposta de sucesso:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Resposta de erro:",
          "content": "HTTP/1.1 500 Internal Server Error or Not Found\n{\n     \"status\": false\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>Chave de 44 posições da NFC-e (mais detalhes aqui: https://www.oobj.com.br/bc/article/como-%C3%A9-formada-a-chave-de-acesso-de-uma-nf-e-nfc-e-de-um-ct-e-e-um-mdf-e-281.html).</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemplo:",
        "content": "https://localhost:3019/nfce/cancel/21190528139664000155550000000002511954797040",
        "type": "json"
      }
    ],
    "filename": "routes/nfceRoute.js",
    "groupTitle": "Nfce",
    "name": "DeleteNfceCancelKey"
  },
  {
    "type": "get",
    "url": "/nfce/echo/:uf",
    "title": "Status de microsserviço",
    "version": "1.0.0",
    "group": "Nfce",
    "description": "<p>Permite checar se determinado microsserviço de UF está ativo ou não.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "pong",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Resposta de sucesso:",
          "content": "HTTP/1.1 200 OK\n{\n  \"ping\": \"pong\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Resposta de erro:",
          "content": "HTTP/1.1 503 Unavailable Service\n{\n     unavailable service\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "uf",
            "description": "<p>UF de consulta.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemplo:",
        "content": "https://localhost:3019/nfce/echo/mg",
        "type": "json"
      }
    ],
    "filename": "routes/nfceRoute.js",
    "groupTitle": "Nfce",
    "name": "GetNfceEchoUf"
  },
  {
    "type": "get",
    "url": "/nfce/status/uf",
    "title": "Status de UF",
    "version": "1.0.0",
    "group": "Nfce",
    "description": "<p>Permite checar o estado operante/não operante de uma determinada UF.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pong",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Resposta de sucesso:",
          "content": "HTTP/1.1 200 OK\n{\n  \"ping\": \"pong\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Resposta de erro:",
          "content": "HTTP/1.1 503 Unavailable Service\n{\nunavailable service\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "uf",
            "description": "<p>UF de consulta.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemplo:",
        "content": "https://localhost:3019/echo/mg",
        "type": "json"
      }
    ],
    "filename": "routes/nfceRoute.js",
    "groupTitle": "Nfce",
    "name": "GetNfceStatusUf"
  },
  {
    "type": "post",
    "url": "/nfce/document/:width",
    "title": "Geração de documento DANFE",
    "version": "1.0.0",
    "group": "Nfce",
    "description": "<p>Geração de documento de DANFE a partir de um xml de nota autorizado.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "File",
            "optional": false,
            "field": "pdf",
            "description": "<p>PDF do documento de DANFE.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Resposta de sucesso:",
          "content": "HTTP/1.1 200 OK\n{\n  \"pdf\": \"Conteúdo binário do arquivo PDF\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Resposta de erro:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n     Internal Server Error\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "width",
            "description": "<p>Largura do papel que será utilizado para impressão do DANFE (48, 80 ou 210 mm).</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "nfeProc",
            "description": "<p>Nota fiscal já assinada e autorizada. Pode ser enviada em formato XML ou JSON.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemplo:",
        "content": "https://localhost:3019/nfce/document/80",
        "type": "json"
      }
    ],
    "filename": "routes/nfceRoute.js",
    "groupTitle": "Nfce",
    "name": "PostNfceDocumentWidth"
  },
  {
    "type": "post",
    "url": "/nfce/process",
    "title": "Autorização de nota",
    "version": "1.0.0",
    "group": "Nfce",
    "description": "<p>Envia uma nota para autorização.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "body",
            "description": "<p>Nota assinada e autorizada com a tag &quot;protNFe&quot; inserida.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Resposta de sucesso:",
          "content": "HTTP/1.1 200 OK\n{\n  \"enviNFe\": {\"NFe\": \"Campos internos da NFC-e\", \"protNFe\": \"Campos internos de autorização\" }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Resposta de erro:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n     Internal Server Error\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "enviNFe",
            "description": "<p>conteúdo do XML da nota.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemplo:",
        "content": "https://localhost:3019/nfce/process",
        "type": "json"
      }
    ],
    "filename": "routes/nfceRoute.js",
    "groupTitle": "Nfce",
    "name": "PostNfceProcess"
  }
] });
