@echo off
echo Starting services
echo.
echo Starting up Gateway service...
cd gateway
set TEST_TYPE=integrated&& start  npm run test
timeout 5
echo Starting up MG service...
cd ..
cd mg
set TEST_TYPE=integrated&& start node .
echo Starting up BA service...
cd ..
cd ba
set TEST_TYPE=integrated&& start node .
echo Starting up Signature service...
cd ..
cd signature
set TEST_TYPE=integrated&& start node .
echo Starting up DANFE service...
cd ..
cd danfe
set TEST_TYPE=integrated&& start node .
pause