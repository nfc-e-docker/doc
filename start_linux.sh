echo "\n"
echo "\tStarting NFC-e platform...\n"
echo "\tStarting up the Gateway..."
cd gateway
TEST_TYPE=normal npm start &
echo "\tStarting up MG service..."
cd ..
cd mg
TEST_TYPE=normal npm start &
echo "\tStarting up BA service..."
cd ..
cd ba
TEST_TYPE=normal npm start &
echo "\tStarting up Signature service..."
cd ..
cd signature
TEST_TYPE=normal npm start &
echo "\tStarting up DANFE service..."
cd ..
cd danfe
TEST_TYPE=normal npm start